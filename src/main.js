import Vue from 'vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.bundle'
import jQuery from 'jquery'
Vue.config.productionTip = false
window.$ = jQuery
new Vue({
  render: h => h(App),
}).$mount('#app')
